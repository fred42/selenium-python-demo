# selenium-python-demo
This is just a demo of Selenium with Python for newbees. With this project you could setup selenium env for dev on linux machines.

| Tools       | Version     |
| ----------- | ----------- |
| Python      | 3.9.11      |
| Firefox     | 91.7.1esr   |
| geckodriver | 0.30.0      |

## How to use

### Install Python3 and make a virtual env
Python version from 3.6 to 3.9 are tested OK. Although I recommend you to install 3.9.11 from the source. If you install Python 3.9 from the source, don't forget to install sqlite-devel zlib-devel libffi-devel openssl-devel xz-devel.
```
# On Centos or Fedora using system default Python3
yum install python3-virtualenv
virtualenv --python="/usr/bin/python3" seleEnv

# Or Compile Python3 by yourself
sudo make install sqlite-devel zlib-devel libffi-devel openssl-devel xz-devel -y
wget https://www.python.org/ftp/python/3.9.11/Python-3.9.11.tar.xz
tar xf Python-3.9.11.tar.xz
./configure --prefix="/usr/local" --enable-optimizations --enable-loadable-sqlite-extensions
make
yum install python3-virtualenv
virtualenv --python="/usr/bin/python3" seleEnv

```
### Download this demo and run it
```
git clone https://git.disroot.org/fred42/selenium-python-demo.git
cd selenium-python-demo
# Enter virtual env and install requirements
souece seleEnv/bin/activate
pip install -r r.txt [-i https://pypi.douban.com/simple/]
# Try remove [...] if your network is good.
# Extract Firefox
tar xjf firefox-91.7.1esr.tar.bz2
# Now you could run it
python main.py
```
### Tips
You might disable the proxy code to run it successfully.

The structure is a mess now but you could see that the dirver(Firefox Browser) is configured in the main function and interacting with the webpage is in the showWebpage function. I hope I have time to make it better.

screen is a useful tool to run long time task script in background.

The ip-api query site is the code is just for test, they have good free and commercial API to use. Plaese don't abuse it.