#!/home/fred/Programs/sele/bin/python
# https://selenium-python.readthedocs.io/
import time, os, random
import logging
from datetime import date
from random import randint

from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
# from selenium.common.exceptions import WebDriverException
# from selenium.common.exceptions import ElementClickInterceptedException
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.firefox.service import Service as FirefoxService
from selenium.webdriver.common.by import By

# logging file
logs_dir = os.path.join(os.path.dirname(__file__), "logs")
if not os.path.exists(logs_dir):
    os.makedirs(logs_dir)
log_file = os.path.join(logs_dir, str(date.today()) + ".log")
logging.basicConfig(filename=log_file, encoding='utf-8', level=logging.INFO, format='%(asctime)s[%(levelname).1s]%(message)s', datefmt='%m-%d %H:%M:%S')


# main function
def showWebpage(driver, url, addr):
    step = 0
    reTry = 0
    while reTry < 8:
        reTry = reTry + 1
        if step == 0:
            try:
                driver.get(url)
            except:
                try:
                    if driver.title != "IP-API.com - Geolocation API":
                        logging.warning("open url failed " + str(reTry))
                        driver.get(url)
                except:
                    logging.warning("open baseURL failed again " + str(reTry))
            try:
                submit_button = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//input[@class='form-control']")))
                step = 5
            except TimeoutException:
                logging.warning("locate submit button failed " + str(reTry))
        if step == 5:
            try:
                i = driver.find_element(By.XPATH, "//input[@class='form-control']")
                i.clear()
                time.sleep(0.5)
                i.send_keys(addr)
                time.sleep(5)
                submit_button.submit()
                WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.XPATH, "//span[text()='\"" + addr + "\"']")))
                time.sleep(10 + randint(5, 10))
                return True
            except:
                logging.warning("query result failed " + str(reTry))
                time.sleep(30)
                return False 
    return False 

    
def main(targetUrl, address): 
    userAgentList = ['Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:68.0) Gecko/20100101 Firefox/68.0',
                'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36',
                'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko',
                'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3314.0 Safari/537.36 SE 2.X MetaSr 1.0',
                'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.3 Safari/537.36',
                'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3719.400 QQBrowser/10.5.3715.400',
                'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36 QIHU 360SE/12.2.1384.0',
                'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:68.0) Gecko/20100101 Firefox/68.0',
                'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 Safari/605.1.15',
                'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36',
                'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36'
                   ]
    
    options = FirefoxOptions()
    # headless
    # options.add_argument("--headless")
    # firefox binary
    options.binary_location = os.path.join(os.path.dirname(__file__), "firefox", "firefox")
    
    # language zh-CN，en-US
    options.set_preference('intl.accept_languages', 'en-US')
    # UA
    uaT = random.choice(userAgentList)
    # logging.warning(uaT)
    options.set_preference("general.useragent.override", uaT)
    # 2 will disable images in page
    options.set_preference('permissions.default.image', 2)
    
    # http proxy
    # http_proxy_ip = "127.0.0.1"
    # http_proxy_port = 44083
    # options.set_preference("network.proxy.type", 1)
    # options.set_preference("network.proxy.http", str(http_proxy_ip))
    # options.set_preference("network.proxy.http_port", int(http_proxy_port))
    # options.set_preference("network.proxy.ssl", str(http_proxy_ip))
    # options.set_preference("network.proxy.ssl_port", int(http_proxy_port))
    
    # socks5 proxy
    socks_proxy_ip = "127.0.0.1"
    socks_proxy_port = 44082
    options.set_preference("network.proxy.type", 1)
    options.set_preference("network.proxy.socks", str(socks_proxy_ip))
    options.set_preference("network.proxy.socks_port", int(socks_proxy_port))
    
    options.set_preference("network.http.use-cache", False)
        
    service = FirefoxService(executable_path=os.path.join(os.path.dirname(__file__), "geckodriver"))
    
    try:
        driver = webdriver.Firefox(options=options, service=service) 
    except:
        logging.warning("driver init failed")
        return False  
    # window size
    # driver.set_window_size(414,736) 
    # page timeout
    driver.set_page_load_timeout(100)
    
    taskResult = showWebpage(driver, targetUrl, address)

    try:
        driver.quit()
    except:
        logging.warning("driver quit failed")
    return taskResult


if __name__ == '__main__':
    urls = """https://ip-api.com/
https://ip-api.com/"""
    addresses = """61.129.8.140
129.211.176.242
125.39.45.245"""

    addressList = addresses.split()
    urlList = urls.split()
    addressIndex = 0
    urlIndex = 0
    for i in range(0, 50 * len(urlList)):
        print(i)
        u = urlList[i % len(urlList)]
        rr = main(u, addressList[addressIndex])
        if rr == False:
            rr = main(u, addressList[addressIndex])
        if rr == False:
            rr = main(u, addressList[addressIndex])
            
        logging.info(str(rr) + "::" + addressList[addressIndex] + ".." + u, "res")
        addressIndex = addressIndex + 1

'''
#yum install xorg-x11-server-Xvfb -y
#geckodriver 0.30.0 and firefox-91.7.1esr
tar xjf firefox-91.7.1esr.tar.bz2
pip install -r r.txt -i https://pypi.douban.com/simple/
#source /data/fred/pythons/p3911/bin/activate
python main.py
'''
